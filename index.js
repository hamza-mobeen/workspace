const express = require('express');  //it will automatically included from our node-modules in our application
const http = require('http');
const morgan = require('morgan');

const hostname = 'localhost';
const port = 3000;

const app = express();   // our app is gonna be used express node module
app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));   // stattic html files will be serve up by my express server

app.use((req, res, next) => {        //to construct our web server using express //next is a middleware of express
 
    // console.log(req.headers);
    res.statusCode=200;
    res.header('Content-Type', 'text/html');
    res.end('<html><body><h1> This is an Express Server </h1></body></html>');

}) ;     

const server = http.createServer(app);
server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}`)
});